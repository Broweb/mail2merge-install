#Mail2Merge; Attach documents from SharePoint directly to your emails.
#Script to register app in Azure AD.
#https://www.Broweb.nl/mail2merge/

$appName = "Mail2Merge"
write-host "$appName, register app in Azure AD."

function AddResourcePermission(
$requiredAccess,
$exposedPermissions,
[string]$requiredAccesses,
[string]$permissionType)
{
    foreach($permission in $requiredAccesses.Trim().Split("|"))
    {
        foreach($exposedPermission in $exposedPermissions)
        {
            if ($exposedPermission.Value -eq $permission)
            {
                $resourceAccess = New-Object Microsoft.Open.AzureAD.Model.ResourceAccess
                $resourceAccess.Type = $permissionType # Scope = Delegated permissions | Role = Application permissions
                $resourceAccess.Id = $exposedPermission.Id # Read directory data
                $requiredAccess.ResourceAccess.Add($resourceAccess)
            }
        }
    }
}

function GetRequiredPermissions([string] $applicationDisplayName,[string] $requiredDelegatedPermissions,[string]$requiredApplicationPermissions,$servicePrincipal)
{
    if ($servicePrincipal)
    {
        $sp = $servicePrincipal
    }
    else
    {
        $sp = Get-AzureADServicePrincipal -Filter "DisplayName eq '$applicationDisplayName'"
    }
    $requiredAccess = New-Object Microsoft.Open.AzureAD.Model.RequiredResourceAccess
    $requiredAccess.ResourceAppId = $sp.AppId 
    $requiredAccess.ResourceAccess = New-Object System.Collections.Generic.List[Microsoft.Open.AzureAD.Model.ResourceAccess]
    if ($requiredDelegatedPermissions)
    {
        AddResourcePermission $requiredAccess -exposedPermissions $sp.Oauth2Permissions -requiredAccesses $requiredDelegatedPermissions -permissionType "Scope"
    }
    if ($requiredApplicationPermissions)
    {
        AddResourcePermission $requiredAccess -exposedPermissions $sp.AppRoles -requiredAccesses $requiredApplicationPermissions -permissionType "Role"
    }
    return $requiredAccess
}

function registerapp(){
    $requiredResourcesAccess = New-Object System.Collections.Generic.List[Microsoft.Open.AzureAD.Model.RequiredResourceAccess]
    $requiredPermissions = GetRequiredPermissions -applicationDisplayName 'Microsoft Graph' -requiredDelegatedPermissions "User.Read|Mail.ReadWrite"
    $requiredResourcesAccess.Add($requiredPermissions) 
    $myApp = New-AzureADApplication -DisplayName $appName -ReplyUrls $replyUrl -Oauth2AllowImplicitFlow 1 -RequiredResourceAccess $requiredResourcesAccess
    write-host "App registration completed" -ForegroundColor Green
    $appid = $myApp.AppId
    write-host "Fill in the clientid: $appid"
    write-host "And the replyUrl: $replyUrl"
    write-host "In Mail2Merge extension popup."
}

$currentPrincipal = New-Object Security.Principal.WindowsPrincipal( [Security.Principal.WindowsIdentity]::GetCurrent() ) 
if (!$currentPrincipal.IsInRole( [Security.Principal.WindowsBuiltInRole]::Administrator )) 
{ 
    write-host "Error: PowerShell is not running as an Administrator." -ForegroundColor Red
    write-host "Please restart script, run as Administrator " -ForegroundColor Red
    exit
}
if (Get-Module -ListAvailable -Name AzureAD) {
    write-host "Module AzureAD is already installed." -ForegroundColor Green
}else{
    write-host "Module AzureAD needs to be installed." -ForegroundColor Yellow
    write-host "Installing module AzureAD"
    Install-Module AzureAD
}

$Credential = Get-Credential -Message "Fill in your sharepoint credentials."
[System.Reflection.Assembly]::LoadWithPartialName('Microsoft.VisualBasic') | Out-Null
$replyUrl = [Microsoft.VisualBasic.Interaction]::InputBox("Enter one of your sharepoint document libarary URL's.", "ReplyURL", "https://yoursite.sharepoint.com/sites/Documents/Bibliotheek%201/Forms/AllItems.aspx")

$connection = Connect-AzureAD -Credential $Credential -ErrorAction SilentlyContinue
try 
{ $var = Get-AzureADTenantDetail } 
catch
{ Write-Host "Connection error."; exit}


if(($myApp = Get-AzureADApplication -Filter "DisplayName eq '$($appName)'"  -ErrorAction SilentlyContinue))
{
    write-host "App $appName is already registered." -ForegroundColor Red
    Add-Type -AssemblyName PresentationCore,PresentationFramework
    $ButtonType = [System.Windows.MessageBoxButton]::YesNo
    $MessageboxTitle = �App is already registered.�
    $Messageboxbody = �Do you want to remove the current app registration?�
    $MessageIcon = [System.Windows.MessageBoxImage]::Warning
    $Result = [System.Windows.MessageBox]::Show($Messageboxbody,$MessageboxTitle,$ButtonType,$messageicon)
    if ($Result -eq 'Yes'){
        wriet-host "Removing app registration."
        Get-AzureADApplication -Filter "DisplayName eq '$($appName)'" | Remove-AzureADApplication
        write-host "App registration removed." -ForegroundColor Green
        registerapp
    }else{
        write-host "Quit registration" -ForegroundColor Red    
    }
}else
{
    registerapp
}

